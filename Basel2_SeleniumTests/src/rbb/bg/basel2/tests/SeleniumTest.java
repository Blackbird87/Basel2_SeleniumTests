package rbb.bg.basel2.tests;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.Test;

import rbb.bg.basel2.takeascreenshot.ScreenShot;

public class SeleniumTest {
	
	private static final Logger LOG = LogManager.getLogger(SeleniumTest.class);
	public static final String AUTO_IT_LOG_IN_DETAILS =  "AutoIT/handleAuthenticationWindow.exe" ; 
	public static final String DRIVER_TYPE = "webdriver.ie.driver" ;
	public static final String DRIVER_LOCATION = "IEDriver/IEDriverServer.exe";
	public static final String URL_ADDRESS = "http://localhost:8090/basel2";
	

	@Test
	public void mainTest() {

		logIn();
		
	}
	
	
	
	
	/**Login Method
	 * Login to Basel2 Application
	 */
	public void logIn()
	{
		WebDriver driver = null ;
		ScreenShot takeAScreenShot = new ScreenShot() ;
		try {
			Runtime.getRuntime().exec(AUTO_IT_LOG_IN_DETAILS);
			System.setProperty(DRIVER_TYPE, DRIVER_LOCATION);
			driver = new InternetExplorerDriver();
			driver.get(URL_ADDRESS);
			Thread.sleep(2000);
			
			
			takeAScreenShot.takeScreenShot(driver);
			LOG.info("Log-in successful to Basel2");
		
		} catch (Exception e) {
			LOG.error("testJenkins method, error details : " + e);
		} finally {
			if(driver!=null)
				driver.close();
		}
	}

}
