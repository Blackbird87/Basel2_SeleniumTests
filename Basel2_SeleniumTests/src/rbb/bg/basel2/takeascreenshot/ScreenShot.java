package rbb.bg.basel2.takeascreenshot;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


public class ScreenShot {

	private static final Logger LOG = LogManager.getLogger(ScreenShot.class);
	public static final String SCREEN_SHOT_DIRECTORY = "C:/SeleniumTestScreenShots";
	
	
	/**Takes a screenshot of the screen
	 * @param driver org.openqa.selenium.WebDriver driver
	 */
	public void takeScreenShot(WebDriver driver)
	{
		try {
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		Date pictureDateAndTime = new Date();
		System.out.println(String.valueOf(pictureDateAndTime));
		FileUtils.copyFile(scrFile, new File(SCREEN_SHOT_DIRECTORY + "/" + String.valueOf(pictureDateAndTime).replaceAll("\\s+","_").replaceAll(":","_")+".jpg"));
		LOG.info("ScreenShot successfully taken");
		
		} catch (IOException e) {
			LOG.error("screenShot method, error details : " + e);
		}
	}
	
}
